import { creatureStore } from './src/CreatureStore';
import { gameHandler } from './src/GameHandler';

const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 5050;

app.use(bodyParser.json());

app.get('/', (request, response) => {
    response.status(200).send('Hello Creature Game Server online!');
});

app.get('/creatures', (request, response) => {
    response.status(200).json(creatureStore.creatures);
});

app.post('/register', (request, response) => {
    if (!request.body || !request.body.name || !request.body.url) {
        response.status(400).send('Invalid payload.');
    }

    const registeredCreature = gameHandler.registerCreature(request.body);

    response.status(201).json(registeredCreature);
});

app.listen(port, () =>{
    console.log(`Example app listening on port ${port}!`);
});
