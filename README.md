# rock-paper-scissors

Small backend game app for a school project

You can access the server via
```
http://creepy-hd.de:8000
```

# API

#### `POST` auf `/register`
Endpoint to register your creature

*Request:*
```json
{
    "name": "myName", 
    "url": "http://myUrl" 
}
```

*Response:* `200`
```json
{
    "position": {
        "x": 5,
        "y": 1
    },
    "gamefield": {
        "width": 100,
        "height": 100
    }
}
```
<br>

#### `GET` auf `/creatures`
Returns a list of all registered `Creatures`. 

*Response:* `200`:
```json
[
    {
        "name": "firstCreature", 
        "url": "http://firstUrl", 
        "position": {
            "x": 5,
            "y": 1
        }
    },
    {
        "name": "secondCreature", 
        "url": "http://secondUrl", 
        "position": {
            "x": 13,
            "y": 9
        }
    }
]
```
<br>
=======



# Documentation Microservice Game (WIP)

Communication is via rest api in json


Creature:
- name
- position
- url
- move() up, down, left, right @/move
- fight() scissor,rock,paper @/fight


API at Creature

/info

response
- name, url

/update

post
- playerlist

/move

post
- moveused(true)

response
- direction -> up, down, left, right

/fight

post
- fight (ture, false)
-moveused(true)

response
- fightmove - > scissor. rock, paper

/logout

response
- logout(true, false)


*********FÜR Server Team************
Gameserver:
- Playerlist (Boardlist: name,location,score,rank)
- Playground bounds
- compareposition()
- addplayer()
- comparefight()
- getmove()
- getfightmove()
- logout()

Visuelle Darstellung

/board

************************************

API at Gameserver for Ceatures


/register

post
- name

response
- position
- playground bounds



/playerlist

response
- creaturelist(of creature objects): name, location, score, place (rank), (last used move)
