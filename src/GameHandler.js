import { GAME_HEIGHT, GAME_WIDTH } from '../configuration';
import { creatureStore } from './CreatureStore';

export const gameHandler = new class GameHandler {
    registerCreature({ name, url }) {
        const { x, y } = randomizePosition();

        return creatureStore.registerCreature({ name, url, x, y });
    }
};

function randomizePosition() {
    return {
        x: (Math.random() * Math.floor(GAME_WIDTH)).toFixed(),
        y: (Math.random() * Math.floor(GAME_HEIGHT)).toFixed(),
    };
};
