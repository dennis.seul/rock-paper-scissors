export const creatureStore = new class CreatureStore {
    constructor() {
        this.creatures = {};
    }

    registerCreature({name, url, x, y}) {
        console.log('<< Creatures >>', this.creatures);
        this.creatures = {
            ...this.creatures,
            [url]: {
                name,
                url,
                position: { x, y },
            },
        };

        return this.creatures[url];
    };

    changeCreaturePosition(url, x, y) {
        this.creatures = {
            ...this.creatures,
            [url]: {
                ...this.creatures[url],
                position: { x, y },
            },
        };

        return this.creatures[url];
    }
};
